import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('aPanel') aPanel: TemplateRef<any>;
  @ViewChild('bPanel') bPanel: TemplateRef<any>;
  @ViewChild('cPanel') cPanel: TemplateRef<any>;
  @ViewChild('dPanel') dPanel: TemplateRef<any>;
  @ViewChild('ePanel') ePanel: TemplateRef<any>;
  @ViewChild('fPanel') fPanel: TemplateRef<any>;
  @ViewChild('gPanel') gPanel: TemplateRef<any>;
  @ViewChild('hPanel') hPanel: TemplateRef<any>;

  highcharts = Highcharts;
   splineChartOptions = {   
      chart: {
        //  styledMode: true,
        //  renderTo : 'container',
         type: "spline",
        //  backgroundColor : {
        //     linearGradient : [0, 0, 0, 400],
        //     stops : [
        //       [0, 'rgb(96, 96, 96)'],
        //       [1, 'rgb(16, 16, 16)']
        //     ]
        //  },
         backgroundColor: '#404d60',
         borderColor:'#404d60',
         borderWidth:0,
         spacingRight: 0,
         spacingLeft: 0,
         marginRight: 0,
        style: {"font-size":10}        
      },
      title: {
         text: "Fails Over 7 Days",
         style: {
          color: '#ffffff',
          fontSize: '13px',
          // fontFamily: 'Overpass'
         }
      },
      subtitle: {
         text: ""
      },
      xAxis:{
         categories:["8/17", "8/18", "8/19", "8/20", "8/21", "8/22", "8/23"],
         labels:{
           style: {
            color: '#838b97',
            fontSize: '10px'
           }
         },
         lineColor: '#7d8591',
         gridLineColor: '#4c586a',
         gridLineWidth: 1.4,
         tickInterval: 1,
         minorTicks: true,
         minorTickInterval: 1,         
         startOnTick: false
      },
      yAxis: {          
         title:{
            text:""
         },
         labels:{
          enabled: false
         },
         visible: true,
         lineColor: '#7d8591',
         gridLineColor: '#4c586a',
         gridLineWidth: 1.4,
         tickInterval: 1.5, 
         minorTicks: true,
         minorTickLength: 0,
         minorTickInterval: 0.1,
         startOnTick: false
      },
      tooltip: {
         valueSuffix:""
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      series: [
         {
            name: '',
            cursor: 'pointer',
            clip: false,
            data: [0.5, 1, 1.3, 3.5, 5.4, 2.1, 1.8],
            color: '#f45b5f',
            lineWidth:1.5,
            marker: {
              enabled: false,
              fillColor: '#404d60',
              lineColor: '#f45b5f'
            },
            // linecap: 'round',
            // type : "area",
            // fillColor : {
            //   linearGradient : [0, 0, 0, 90],
            //   stops : [
            //     [0, 'rgba(243,55,59,1)'],
            //     [1, 'rgba(2,0,0,0)']
            //   ]
            // }
         }
      ]
   };

   barChartOptions = {
    chart: {
        type: 'column',
        backgroundColor: '#404d60',
        borderColor:'#404d60',
        borderWidth:0,
        spacingRight: 0,
        spacingLeft: 0,
        marginRight: 0,
        style: {"font-size":10} 
    },
    title: {
        text: 'Volume',
        style: {
          color: '#ffffff',
          fontSize: '13px'
        }
    },
    subtitle: {
        text: ''
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis: {
        visible: true,
        labels:{
          enabled: false
        },
        lineColor: '#7d8591',
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ]
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        },
        labels:{
          enabled: false
        },
        gridLineColor: '#404d60'
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            borderRadius: '5px'
        }
    },
    series: [{
        name: '',
        data: [149.9, 71.5, 206.4, 129.2, 144.0, 276.0, 135.6, 348.5, 216.4, 194.1, 95.6, 54.4]

    }]
}
  constructor() { 
    
  }
  all = [];
  even = [];

  id: number = 7;
  row: number = Math.ceil(this.id / 4);
  row1 = Array(this.row).fill(1);
  id1 = Array(4).fill(0).map((_, index) => index + 1);
  row2 = Array(this.id).fill(1).map((_, index) => index + 1);
  widgetMatrix = [];

  widgets = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h'
  ];

  dropDemo1(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
      event.previousContainer.data.push(event.container.data.pop());
    }
  }

  ngOnInit() {
    var chunk_size = 4;
    var arr = this.widgets;
    var groups = arr.map(function (e, i) {
      return i % chunk_size === 0 ? arr.slice(i, i + chunk_size) : null;
    })
      .filter(function (e) { return e; });
    this.widgetMatrix = groups;
    console.log("TCL: DashboardComponent -> ngOnInit -> this.matrix", this.widgetMatrix)
    this.all = this.widgetMatrix[0];
    this.even = this.widgetMatrix[1];
  }
}